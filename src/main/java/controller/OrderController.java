package controller;

import dao.OrderDao;
import models.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderDao dao;

    @GetMapping("orders")
    public List<Order> findAll() {
        return dao.findAll();
    }

    @GetMapping("orders/{id}")
    public Order findById(@PathVariable("id") Long id) {
        return dao.findById(id);
    }

    @PostMapping("orders")
    @ResponseBody
    public @Valid Order save(@RequestBody @Valid Order order) {
        dao.save(order);
        return order;
    }

    @DeleteMapping("orders/{id}")
    public void delete(@PathVariable("id") Long id) {
        dao.delete(id);
    }

    @DeleteMapping("orders")
    public void deleteAll() {
        dao.deleteAll();
    }
}
