package dao;

import models.Order;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Primary
@Repository
@Transactional
public class OrderHsqlDao implements OrderDao {

    @PersistenceContext
    private EntityManager em;

    public Order findById(Long id) {
        return em.find(Order.class, id);
    }

    public List<Order> findAll() {
        TypedQuery<Order> query = em.createQuery("select o from Order o", Order.class);
        return query.getResultList();
    }

    public Order save(Order order) {
        if (order.getId() == null) {
            em.persist(order);
        } else {
            em.merge(order);
        }
        return order;
    }

    public void delete(Long id) {
        Order order = em.find(Order.class, id);
        em.remove(order);
    }

    public void deleteAll() {
        em.createQuery("delete from Order").executeUpdate();
    }
}