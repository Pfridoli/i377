package dao;

import models.Order;

import java.util.List;

public interface OrderDao {

    Order findById(Long id);

    Order save(Order order);

    List<Order> findAll();

    void delete(Long id);

    void deleteAll();

}
