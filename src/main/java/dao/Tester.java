package dao;

import conf.DbConfig;
import models.Order;
import models.OrderRow;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class Tester {
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx =
                new AnnotationConfigApplicationContext(DbConfig.class);

        OrderDao dao = ctx.getBean(OrderDao.class);

        Order order = new Order("153321");
        order.getOrderRows().add(new OrderRow("test1", 1, 1));
        order.getOrderRows().add(new OrderRow("test2", 2, 2));

        Order order2 = new Order("7667865");
        order2.getOrderRows().add(new OrderRow("test777", 4, 8));

        dao.save(order);
        dao.save(order2);

        System.out.println(dao.findById((long) 1));
        System.out.println(dao.findAll());

        dao.delete((long) 1);
        System.out.println(dao.findById((long) 1));

        dao.deleteAll();
        System.out.println(dao.findAll());

        ctx.close();

    }
}
